<?php

require_once 'vendor/autoload.php';

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

$connection = new AMQPStreamConnection('13.232.208.10', 5672, 'MoRD', 'G3CpQKMoRD', 'sdmstpmintegration');
$channel = $connection->channel();

$queue = 'NxtGenSDMS_SubmitRequest';

$msg = file_get_contents('reqJson.json');
$jsonPayload = json_encode($msg);

$message = new AMQPMessage($jsonPayload);
$channel->basic_publish($message, '', $queue);

echo " [x] Sent Payload\n";

$channel->close();
$connection->close();