<?php

require_once 'vendor/autoload.php';

use PhpAmqpLib\Connection\AMQPStreamConnection;

$connection = new AMQPStreamConnection('13.232.208.10', 5672, 'MoRD', 'G3CpQKMoRD', 'sdmsResponse_MoRD');
$channel = $connection->channel();

$queue = 'NxtGenSDMS_GetResponse_MoRD';

echo " [*] Waiting for messages. To exit press CTRL+C\n";

$callback = function ($msg) {
    echo ' [x] Received ', $msg->body, "\n";
};
  
$channel->basic_consume($queue, '', false, true, false, false, $callback);
  
while (count($channel->callbacks)) {
    $channel->wait();
}